using UnityEngine;
using System;

public class FGNonConsumableItem {
	public delegate void OnHitDelegate(FGGameBlock target);
	public delegate void OnHitByDelegate(FGGameBlock hitBy);
	public event OnHitDelegate OnHit;
	public event OnHitByDelegate OnHitBy;

	public void Hit(FGGameBlock gameBlock) {
		if (OnHit != null) {
			OnHit(gameBlock);
		}
	}

	public void GetHitBy(FGGameBlock gameBlock) {
		if (OnHitBy != null) {
			OnHitBy(gameBlock);
		}
	}
}
