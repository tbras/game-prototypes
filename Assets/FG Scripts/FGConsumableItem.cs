using UnityEngine;
using System;

public class FGConsumableItem {
	public delegate void OnConsumeItemDelegate(FGConsumableItem item, FGCharacter character);
	public event OnConsumeItemDelegate OnConsumeItem; 

	public void ConsumeItem(FGCharacter character) {
		if (OnConsumeItem != null) {
			OnConsumeItem(this, character);
		}
	}
}
