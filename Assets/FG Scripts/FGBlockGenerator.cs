using UnityEngine;
using System;
using Weighted_Randomizer;

public class BlockGenerator {
	// Block weights
	public int SwordBlockWeight = 3;
	public int ShieldBlockWeight = 2;
	public int GoldBlockWeight = 1;
	public int ManaBlockWeight = 2;
	public int HealthBlockWeight = 1;
	public int ExperienceBlockWeight = 1;
	
	private IWeightedRandomizer<BlockType> _randomizer;
	
	public BlockGenerator() {
		_randomizer = new StaticWeightedRandomizer<BlockType>();
		_randomizer.Add(BlockType.Sword, SwordBlockWeight);
		_randomizer.Add(BlockType.Shield, ShieldBlockWeight);
		_randomizer.Add(BlockType.Gold, GoldBlockWeight);
		_randomizer.Add(BlockType.Mana, ManaBlockWeight);
		_randomizer.Add(BlockType.Health, HealthBlockWeight);
		_randomizer.Add(BlockType.Experience, ExperienceBlockWeight);
	}
	
	public GameBlock NextBlock () {
		BlockType type = _randomizer.NextWithReplacement();
		Vector3 position = Vector3.zero;

		if (type == BlockType.Sword) {
			return PrefabsManager.instance.blocksPrefabs.swordPrefab.Spawn(position);
		} else if (type == BlockType.Shield) {
			return PrefabsManager.instance.blocksPrefabs.shieldPrefab.Spawn(position);
		} else if (type == BlockType.Gold) {
			return PrefabsManager.instance.blocksPrefabs.goldPrefab.Spawn(position);
		} else if (type == BlockType.Mana) {
			return PrefabsManager.instance.blocksPrefabs.manaPrefab.Spawn(position);
		} else if (type == BlockType.Health) {
			return PrefabsManager.instance.blocksPrefabs.healthPrefab.Spawn(position);
		} else if (type == BlockType.Experience) {
			return PrefabsManager.instance.blocksPrefabs.experiencePrefab.Spawn(position);
		} else {
			throw new UnityException("Invalid BLOCK!");
		}
	}
}


