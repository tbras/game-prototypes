using System;
using UnityEngine;
using UnityPatterns;
using System.Collections;

public enum BlockType { None, Sword, Shield, Gold, Mana, Experience, Health }


public class GameBlock : MonoBehaviour {
	public BlockType blockType;
	
	private Vector3 _velocity;
	private bool _isMoving;
	private AnimationCurve _motionCurve;
	
	public bool IsAnimating {
		get { return _isMoving; }
	}

	public Vector3 LocalPosition {
		get { return transform.localPosition; }
		set { transform.localPosition = value; }
	}

	public IEnumerator coMoveTo(Vector3 targetPosition) {
		_isMoving = true;

		yield return StartCoroutine(transform.MoveTo(targetPosition, 0.3f));

		_isMoving = false;
	}

	public IEnumerator coShakeHorizontally(int amount, float duration = 0.5f, bool moveLeftFirst = true) {
		_isMoving = true;
		float size = renderer.bounds.size.x / 6f;

		Vector3 left = transform.localPosition;
		left.x -= size;
		Vector3 center = transform.localPosition;
		Vector3 right = transform.localPosition;
		right.x += size;

		Vector3[] locations = null;

		if (moveLeftFirst) { 
			locations = new Vector3[]{left, right}; 
		} else { 
			locations = new Vector3[]{right, left}; 
		}

		for (int i = 0; i < amount; i++) {
			for (int j = 0; j < locations.Length; j++) {
				yield return StartCoroutine(transform.MoveTo(locations[j], (duration / (amount + 1))));
			}
		}

		yield return StartCoroutine(transform.MoveTo(center, (duration / (amount + 1))));

		_isMoving = false;
	}

	// Use this for initialization
	void Start () {
		_motionCurve = new AnimationCurve(new Keyframe(0, 0), new Keyframe(0.7f, 0.9f), new Keyframe(1, 1));
	}

	void Update () {
				
	}

	public void SetColliderSize(Vector3 size) {
		BoxCollider box = collider as BoxCollider;

		box.size = size;
	}

	public virtual bool IsMatch(GameBlock block) {
		return (blockType == block.blockType);
	}

	public virtual void Dispose() {
		this.Recycle();
	}
}

