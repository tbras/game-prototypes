using UnityEngine;
using UnityPatterns;
using System;
using System.Collections;

public class FGCharacter {
	public delegate void OnAttackDelegate(FGCharacter defender);
	public delegate void OnKilledDelegate(FGCharacter attacker);
	public event OnAttackDelegate OnAttack;
	public event OnKilledDelegate OnKilled;

	private int _attack;
	private int _health;
	private int _numberOfSteps;
	private tk2dTextMesh _attackTextMesh;
	private tk2dTextMesh _healthTextMesh;
	private tk2dTextMesh _numberOfStepsTextMesh;

	private FGGameBlock _parentBlock;

	public FGGameBlock ParentBlock {
		get { return _parentBlock; }
		set { _parentBlock = value; }
	}

	public int Attack {
		get { return _attack; }
		set { 
			_attack = Mathf.Clamp(value, 0, 99); 

			if (_attackTextMesh != null) {
				_attackTextMesh.text = _attack.ToString();
				_attackTextMesh.Commit();
			}
		}
	}

	public int Health {
		get { return _health; }
		set { 
			_health = Mathf.Clamp(value, 0, 99);

			if (_healthTextMesh != null) {
				_healthTextMesh.text = _health.ToString();
				_healthTextMesh.Commit();
			}
		}
	}

	public int NumberOfSteps {
		get { return _numberOfSteps; }
		set { 
			_numberOfSteps = Mathf.Clamp(value, 0, 10);
			
			if (_numberOfStepsTextMesh != null) {
				_numberOfStepsTextMesh.text = _numberOfSteps.ToString();
				_numberOfStepsTextMesh.Commit();
			}
		}
	}

	public FGCharacter(int attack, int health) {
		this.Attack = attack;
		this.Health = health;
	}

	public IEnumerator AttackCharacter(FGCharacter defender, int bonus = 0) {
		if (!(ParentBlock.spriteId == FGSpriteID.CharBloody || defender.ParentBlock.spriteId == FGSpriteID.CharBloody)) {

		} else {
			defender.Health = defender.Health - _attack - bonus;

			defender.ParentBlock.StartCoroutine(defender.ParentBlock.coShakeHorizontally(3, 0.3f));

			if (defender.Health <= 0) {
				if (OnKilled != null) {
					defender.OnKilled(this);
				}

				yield return defender.ParentBlock.StartCoroutine(defender.ParentBlock.transform.ScaleTo(Vector3.zero, 0.6f));
				
				FGGameBoard.instance.RemoveBlock(defender.ParentBlock);

				defender.ParentBlock.Dispose();
			}
		}

		yield return null;
	}

	public void AddAttackTextMeshObserver(tk2dTextMesh textMesh) {
		_attackTextMesh = textMesh;
	}

	public void AddHealthTextMeshObserver(tk2dTextMesh textMesh) {
		_healthTextMesh = textMesh;	
	}

	public void UpdateTextMeshes() {
		_attackTextMesh.text = _attack.ToString();
		_healthTextMesh.text = _health.ToString();
	}
}
