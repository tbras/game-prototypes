using System;
using UnityEngine;

public abstract class BaseBuff {
	private int _duration; 
	private int _currDuration; 

	private BaseHero _caster;
	private BaseHero _opponent;
	private GameBoard _gameBoard;

	private bool hasBeenDeactivated = false;

	protected BaseBuff(int duration, BaseHero caster, BaseHero opponent, GameBoard gameBoard) {
		Duration = duration;
		CurrDuration = duration;

		_caster = caster;
		_opponent = opponent;
		_gameBoard = gameBoard;

		OnActivation(caster, opponent, gameBoard);
	}

	#region Public API

	public void PassTurn() {
		CurrDuration--;

		if (CurrDuration <= 0 && !hasBeenDeactivated) {
			hasBeenDeactivated = true;

//			OnDeactivation();
		}
	}

	public abstract void OnActivation(BaseHero caster, BaseHero opponent, GameBoard gameBoard);
	public abstract void OnDeactivation(BaseHero caster, BaseHero opponent, GameBoard gameBoard);

	#endregion
	
	#region Getters and Setters

	public int Duration {
		get { return _duration; }
		set { _duration = Mathf.Clamp(value, 1, int.MaxValue); }
	}

	public int CurrDuration {
		get { return _currDuration; }
		set { _currDuration = Mathf.Clamp(value, 0, _duration); }
	}

	#endregion
}


