﻿using UnityEngine;
using System.Collections;

public class TestEvents : MonoBehaviour {

	public delegate void ClickDelegate();
	public static event ClickDelegate OnClicked;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public static void SubscribeOnClicked(ClickDelegate func) {
		OnClicked += func;
	}

	public static void UnsubscribeOnClicked(ClickDelegate func) {
		OnClicked -= func;
	}

	void OnGUI() {
		if (GUI.Button(new Rect(0, 0, 50, 30), "Click")) { 
			if (OnClicked != null) 
				OnClicked();
		}
	}
}
