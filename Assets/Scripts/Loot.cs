﻿using UnityEngine;
using System.Collections;
using Weighted_Randomizer;

enum Attack {Miss, Hit};

public class Loot : MonoBehaviour {

	// Use this for initialization
	void Start () {

		return;

		IWeightedRandomizer<Attack> rand = new StaticWeightedRandomizer<Attack>();

		rand.Add(Attack.Hit, 90);
		rand.Add(Attack.Miss, 10);

		for (int i = 0; i < 10; i++) {
			Debug.Log(rand.NextWithReplacement().ToString());
		}

		//StartCoroutine(Say("Hello World!", 3f));
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator Say(string msg, float interval) {

		while (true) {
			Debug.Log(msg);

			yield return new WaitForSeconds(interval);
		}
	}
}
