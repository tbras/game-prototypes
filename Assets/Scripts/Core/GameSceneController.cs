﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameSceneController : MonoBehaviour {
	private enum GameSceneState { WaitingForMove, CastingSpell };

	public GameBoard gameBoard; 
	public Player playerOne;
	public Player playerTwo;

	private Player _currentPlayer;
	private Player _nextPlayer;
	private GameSceneState _state = GameSceneState.WaitingForMove;
	private Color _activePlayerColor;

	void Awake() {
		_activePlayerColor = new Color(27f / 80f, 19f / 63f, 13 / 42f);
	}

	// Use this for initialization
	void Start () {
		gameBoard.OnBlockSelection += OnBlockSelection_Forward;
//		gameBoard.OnCombos += OnCombos_Forward;
		gameBoard.coOnCombosPreGravity += coOnCombosPreGravity;
		gameBoard.coOnCombosPosGravity += coOnCombosPosGravity;

		gameBoard.OnSwappingBlocksCombosChecked += OnSwappingBlocksCombosChecked_Forward;

		StartCoroutine(coStartGame(playerOne));
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey(KeyCode.Escape)) {
//			Application.Quit();	
			Application.LoadLevel(Application.loadedLevel);
		} 
	}

	#region Getters and Setters

	public Player CurrentPlayer {
		get { return _currentPlayer; }
	}

	public Player NextPlayer {
		get { return _nextPlayer; }
		set { _nextPlayer = value; }
	}

	#endregion

	#region Public API

	public Player GetPlayerOpponent(Player player) {
		return player.Equals(playerOne) ? playerTwo : playerOne;
	}

	#endregion

	private void OnBlockSelection_Forward(BoardPosition position) {
		StartCoroutine(coOnBlockSelection(position));
	}

	// This is called after all the combos resulted from a User Swap Motion, ie, end of turn
	private void OnSwappingBlocksCombosChecked_Forward() {
		StartCoroutine(coOnSwappingBlocksCombosChecked());
	} 

	private void UpdateTextMeshes() {
		_currentPlayer.heroStats.nameTextMesh.color = _activePlayerColor;
		_currentPlayer.heroStats.nameTextMesh.Commit();
		
		GetPlayerOpponent(_currentPlayer).heroStats.nameTextMesh.color = Color.white;
		GetPlayerOpponent(_currentPlayer).heroStats.nameTextMesh.Commit();
	}

	#region Routines

	private IEnumerator coStartGame(Player firstPlayer) {
		while (!gameBoard.IsSwapEnable()) {
			yield return null;
		}
		
		_currentPlayer = firstPlayer;

		UpdateTextMeshes();

		yield return StartCoroutine(_currentPlayer.coOnStartTurn(this));
	}

	private IEnumerator coOnBlockSelection(BoardPosition position) {
		yield return StartCoroutine(_currentPlayer.coOnBlockSelection(this, position));
	}
	
	private IEnumerator coOnCombosPreGravity(List<Combo> combos) {
		yield return StartCoroutine(_currentPlayer.coOnCombosPreGravity(this, combos));
	}

	private IEnumerator coOnCombosPosGravity(List<Combo> combos) {
		yield return StartCoroutine(_currentPlayer.coOnCombosPosGravity(this, combos));
	}
	
	// This is called after all the combos resulted from a User Swap Motion, ie, end of turn
	private IEnumerator coOnSwappingBlocksCombosChecked() {
		// We set this to null to check it later if it was change by any buff/spell/etc...
		_nextPlayer = null;
		
		yield return StartCoroutine(_currentPlayer.coOnEndTurn(this));
		
		if (_nextPlayer == null) {
			_nextPlayer = GetPlayerOpponent(_currentPlayer);
		}
		
		_currentPlayer = _nextPlayer;

		UpdateTextMeshes();
		
		yield return StartCoroutine(_currentPlayer.coOnStartTurn(this));
	} 

	#endregion
}
