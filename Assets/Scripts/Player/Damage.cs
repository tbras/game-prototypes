using UnityEngine;
using System;

public enum DamageType { White, WhiteCrit, Spell, SpellCrit, None };

public class Damage {
	private int _amount;
	private DamageType _type;

	public Damage() : this(0, DamageType.None) {

	}

	public Damage(int amount, DamageType type) {
		Amount = amount;
		_type = type;
	}

	public int Amount {
		get { return _amount; }
		set { _amount = Mathf.Clamp(value, 0, int.MaxValue); }
	}

	public DamageType Type {
		get { return _type; }
		set { _type = value; }
	}
}


