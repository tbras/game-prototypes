﻿using UnityEngine;
using System.Collections;

public class UIProgressBar : MonoBehaviour {	
	public int maxValue = 10;
	public int currValue = 5;
	public bool showText = true;
	public bool showOnlyCurrValue = false;
	public string units = "";

	private tk2dUIProgressBar _progressBar;
	private tk2dTextMesh _valueTextMesh;

	private bool _isDirty = true;

	// Use this for initialization
	void Start () {
		_progressBar = GetComponentInChildren<tk2dUIProgressBar>();
		Transform textMeshTransform = transform.FindChild("TextMeshes");

		if (textMeshTransform != null) {
			_valueTextMesh = textMeshTransform.Find("Value").GetComponentInChildren<tk2dTextMesh>();
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (_isDirty) {
			_isDirty = false;

			UpdateProgressBar();

			if (showText) {
				if (_valueTextMesh != null) {
					_valueTextMesh.renderer.enabled = true;
				}
			} else {
				if (_valueTextMesh != null) {
					_valueTextMesh.renderer.enabled = false; 
				}
			}
		}
	}

	public int MaxValue {
		get { return maxValue; }
		set { 
			maxValue = value; 
			_isDirty = true;
		}
	}

	public int CurrValue {
		get { return currValue; }
		set { 
			currValue = value;
			_isDirty = true;
		}
	}

	public bool ShowText {
		get { return showText; }
		set { 
			showText = value; 
			_isDirty = true;
		}
	}

	private void UpdateProgressBar() {
		_progressBar.Value = (float)currValue / (float)maxValue;

		if (_valueTextMesh != null) {
			_valueTextMesh.text = currValue + units + (showOnlyCurrValue ? "" : " / " + maxValue + units);
		}
	}
}
