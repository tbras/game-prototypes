﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public class Player : MonoBehaviour {
	#region Public Fields
	
	public UIHeroStats heroStats;
	public UIPlayerSpellSlots spellSlots;
	public bool isHuman = true;
	public bool isTestingEnabled = true;

	#endregion

	#region Private Fields

	private BaseHero _hero;
	private HeroBrain _brain;

	private Player _opponent;

	private AudioManager _audioManager;

	#endregion

	void InitializeFromJSONFile(string filePath) {

	}

	void Awake() {
		var equipment = new Equipment(new Weapon("Short Sword", 1, 3));
		
		var names = new string[]{"Tyrion", "Cersei", "Jaime", "Arya", "Dog", "Sansa", 
			"Brienne", "Joffrey", "Bran", "Stannis", "Jon Snow", "Daenerys", "Barristan", "Frey"};
		
		_hero = new HeroWarrior(names[Random.Range(0, names.Length-1)],
		                        new HeroStats(Random.Range(50, 100), Random.Range(10, 20), Random.Range(1, 5), Random.Range(1, 5)), 
		                        equipment);

		heroStats.SubscribeHeroStatsChanges(_hero.Stats);
		heroStats.OnButtonUp += OnUIHeroStatsButtonUp;

//		spellSlots.spellSlot1.SetSpell(new SpellFireBall("Fireball", 6, 1, 4, 6));

		_audioManager = GameObject.Find("AudioManager").GetComponent<AudioManager>();

		_brain = new HeroBrain(_hero);
	}

	// Use this for initialization
	void Start () {
		GameSceneController controller = (GameSceneController)GameObject.FindWithTag(Tags.GameSceneController).GetComponent<GameSceneController>();

		_opponent = controller.GetPlayerOpponent(this);

		heroStats.UpdateComponent(_hero);
		heroStats.SubscribeHeroStatsChanges(_hero.Stats);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	// TODO: This is only for TESTING!!!
	private void OnUIHeroStatsButtonUp() {
		if (!isTestingEnabled) {
			return;
		}

		// Toggles players humanity
		isHuman = !isHuman;

		if (!isHuman) {
			GameSceneController controller = (GameSceneController)GameObject.FindWithTag(Tags.GameSceneController).GetComponent<GameSceneController>();

			StartCoroutine(_brain.coPlay(controller));
		}
	}

	#region Routines (Public API)

	public IEnumerator coOnStartTurn(GameSceneController gameSceneController) {
		if (!isHuman) {
			spellSlots.SetVisibility(false);
			_opponent.spellSlots.SetVisibility(true);

			// If it has a brain, disable user interaction
			gameSceneController.gameBoard.DisableUserInteration();

			_hero.StartTurn(_opponent.Hero, gameSceneController.gameBoard);


			yield return StartCoroutine(_brain.coPlay(gameSceneController));
		} else {
			spellSlots.SetVisibility(true);
			_opponent.spellSlots.SetVisibility(false);

			if (!_opponent.isHuman) {
				spellSlots.SetVisibility(true);

			} 

			gameSceneController.gameBoard.EnableUserInteration();
		}
	}

	public IEnumerator coOnEndTurn(GameSceneController gameSceneController) {
		_hero.EndTurn(_opponent.Hero, gameSceneController.gameBoard);

		yield return null;
	}

	public IEnumerator coOnCombosPreGravity(GameSceneController gameSceneController, List<Combo> combos) {
		HeroStats stats = _hero.Stats;

		const float COMBO_MULTIPLIER = 0.5f;
		const float TIER_MULTIPLIER = 0.25f;

		for (int i = 0; i < combos.Count; i++) {
			Combo combo = combos[i];
			int posDelta = (combo.Positions.Count - 3);

			// Sword
			if (combo.Type == BlockType.Sword) {
				stats.Atk += 1 + posDelta;
			} 
			// Shield
			else if (combo.Type == BlockType.Shield) {
				stats.Def += 1 + posDelta;
			} 
			// Health
			else if (combo.Type == BlockType.Health) {
				float regenAmount = (stats.HealRegenMultiplier * stats.HP);
				regenAmount *= 1.0f + posDelta * COMBO_MULTIPLIER;			// Buff 1
				regenAmount *= 1.0f + (combo.Tier - 1) * TIER_MULTIPLIER;	// Buff 2

				_hero.Heal(Mathf.RoundToInt(regenAmount), _opponent.Hero, gameSceneController.gameBoard);
			}
			// Mana
			else if (combo.Type == BlockType.Mana) {
				float regenAmount = (stats.ManaRegenMultiplier * stats.MP);
				regenAmount *= 1.0f + posDelta * COMBO_MULTIPLIER;			// Buff 1
				regenAmount *= 1.0f + (combo.Tier - 1) * TIER_MULTIPLIER;	// Buff 2

				_hero.RegenMana(Mathf.RoundToInt(regenAmount), _opponent.Hero, gameSceneController.gameBoard);
			}
			// Gold
			else if (combo.Type == BlockType.Gold) {

			}
			// Experience
			else if (combo.Type == BlockType.Experience) {
				_opponent.Hero.Stats.CombosCounter--;
			}

			_audioManager.PlaySoundEffect(SoundEffect.ComboGeneric);

			// Update combo to attack counter
			stats.CombosCounter++;

			if (stats.CombosCounter >= stats.CombosToAttack) {
				_hero.Attack(_opponent.Hero, gameSceneController.gameBoard);

				_audioManager.PlaySoundEffect(SoundEffect.AttackSword);

				stats.CombosCounter = 0;
			}
		}

		yield return null;
	}
	
	public IEnumerator coOnCombosPosGravity(GameSceneController gameSceneController, List<Combo> combos) {
		for (int i = 0; i < combos.Count; i++) {
			Combo combo = combos[i];

			if (combo.Type == BlockType.Gold) {
				gameSceneController.gameBoard.ReplaceRandomBlockByRandomBlock();
			}
		}

		yield return null;
	}

	public IEnumerator coOnBlockSelection(GameSceneController gameSceneController, BoardPosition position) {
		yield return null;
	}

	#endregion

	#region Getters 

	public BaseHero Hero {
		get { return _hero; }
	}

	public HeroBrain Brain {
		get { return _brain; }
		set { _brain = value; }
	}

	#endregion
}
