﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class UIHeroStats : MonoBehaviour {
	public tk2dTextMesh nameTextMesh;
	public tk2dTextMesh levelTextMesh;
	public tk2dTextMesh attackTextMesh;
	public tk2dTextMesh defenseTextMesh;
	public UIProgressBar healthBar;
	public UIProgressBar manaBar;
	public UIProgressBar combosCounter;

	public event Action OnButtonUp;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown(0)) {
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			
			if (collider.Raycast(ray, out hit, 1000f) && OnButtonUp != null) {
				OnButtonUp();
			}
		}
	}

	public void SubscribeHeroStatsChanges(HeroStats heroStats) {
		heroStats.OnHPChange += SetHP;
		heroStats.OnCurrentHPChange += SetCurrentHP;
		heroStats.OnMPChange += SetMP;
		heroStats.OnCurrentMPChange += SetCurrentMP;
		heroStats.OnLevelChange += SetLevel;
		heroStats.OnAtkChange += SetAtk;
		heroStats.OnDefChange += SetDef;
		heroStats.OnCombosCounterChange += SetCurrentCombosCounter;
		heroStats.OnCombosToAttackChange += SetCombosToAttack;
	}

	public void UpdateComponent(BaseHero hero) {
		SetName(hero.Name);
		SetLevel(hero.Stats.Level);
		SetDef(hero.Stats.Def);
		SetCurrentHP(hero.Stats.CurrentHp);
		SetHP(hero.Stats.HP);
		SetCurrentMP(hero.Stats.CurrentMp);
		SetMP(hero.Stats.MP);
		SetCombosToAttack(hero.Stats.CombosToAttack);
		SetCurrentCombosCounter(hero.Stats.CombosCounter);
	}

	#region Setters

	public void SetName(string name) {
		nameTextMesh.text = name;
	}

	public void SetLevel(int level) {
		levelTextMesh.text = level.ToString();
	}

	public void SetAtk(int attack) {
		attackTextMesh.text = attack.ToString();
	}

	public void SetDef(int defense) {
		defenseTextMesh.text = defense.ToString();
	}

	public void SetHP(int maxHealth) {
		healthBar.MaxValue = maxHealth;
	}

	public void SetCurrentHP(int currHealth) {
		healthBar.CurrValue = currHealth;
	}

	public void SetMP(int maxMana) {
		manaBar.MaxValue = maxMana;
	}
	
	public void SetCurrentMP(int currMana) {
		manaBar.CurrValue = currMana;
	}

	public void SetCurrentCombosCounter(int amount) {
		combosCounter.CurrValue = amount;
	}

	public void SetCombosToAttack(int amount) {
		combosCounter.MaxValue = amount;
	}

	/// <summary>
	/// Increments the combos counter.
	/// </summary>
	/// <returns><c>true</c>, if current combos reached its max and was reseted to 0, <c>false</c> otherwise.</returns>
	public bool IncCombosCounter() {
		combosCounter.CurrValue += 1;

		if (combosCounter.CurrValue >= combosCounter.MaxValue) {
			combosCounter.CurrValue = 0;

			return true;
		} 

		return false;
	}

	#endregion
}
