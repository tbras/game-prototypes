using UnityEngine;
using System;
using SimpleJSON;

[Serializable]
public class HeroStats {
	private const int MAX_LEVEL = 100; 

	#region Fields

	[SerializeField] private int _level = 1;
	[SerializeField] private int _hp = 10;
	[SerializeField] private int _currentHp;
	[SerializeField] private int _mp = 10;
	[SerializeField] private int _currentMp;
	[SerializeField] private int _atk = 1;
	[SerializeField] private int _def = 1;
	[SerializeField] private int _xp = 0;
	[SerializeField] private float _critChance = 0.10f;
	[SerializeField] private float _critMultiplier = 2.0f;
	[SerializeField] private float _spellCritChance = 0.10f;
	[SerializeField] private float _spellCritMultiplier = 2.0f;
	[SerializeField] private float _healRegenMultiplier = 0.1f;
	[SerializeField] private float _manaRegenMultiplier = 0.1f;
	[SerializeField] private int _combosCounter = 0;
	[SerializeField] private int _combosToAttack = 10;

	#endregion
	
	// Use this for initialization
	public HeroStats(int hp, int mp, int atk, int def, 
	                 float critChance = 0.10f, float critMultiplier = 2.0f,
	                 float spellCritChance = 0.10f, float spellCritMultiplier = 2.0f, 
	                 int xp = 0) {
		HP = hp;
		MP = mp;
		Atk = atk;
		Def = def;
		CritChance = critChance;
		CritMultiplier = critMultiplier;
		SpellCritChance = spellCritChance;
		SpellCritMultiplier = spellCritMultiplier;
		Xp = xp;

		CurrentHp = hp;
		CurrentMp = mp;
	}

	#region Events

	public event Action<int> OnLevelChange;
	public event Action<int> OnHPChange;
	public event Action<int> OnCurrentHPChange;
	public event Action<int> OnMPChange;
	public event Action<int> OnCurrentMPChange;
	public event Action<int> OnAtkChange;
	public event Action<int> OnDefChange;
	public event Action<int> OnXPChange;
	public event Action<float> OnCritChanceChange;
	public event Action<float> OnCritMultiplierChange;
	public event Action<float> OnSpellCritChanceChange;
	public event Action<float> OnSpellCritMultiplierChange;
	public event Action<int> OnCombosCounterChange;
	public event Action<int> OnCombosToAttackChange;

	#endregion

	#region Getters and Setters

	public int Level {
		get { return _level; }
		set { 
			_level = Mathf.Clamp(value, 1, MAX_LEVEL);

			if (OnLevelChange != null) {
				OnLevelChange(_level);
			}
		}
	}

	public int HP {
		get { return _hp; }
		set { 
			_hp = Mathf.Clamp(value, 1, int.MaxValue); 

			if (OnHPChange != null) {
				OnHPChange(_hp);
			}
		}
	}
	
	public int MP {
		get { return _mp; }
		set { 
			_mp = Mathf.Clamp(value, 1, int.MaxValue); 

			if (OnMPChange != null) {
				OnMPChange(_mp);
			}
		}
	}
	
	public int Atk {
		get { return _atk; }
		set { 
			_atk = Mathf.Clamp(value, 0, int.MaxValue); 

			if (OnAtkChange != null) {
				OnAtkChange(_atk);
			}
		}
	}
	
	public int Def {
		get { return _def; }
		set { 
			_def = Mathf.Clamp(value, 0, int.MaxValue); 

			if (OnDefChange != null) {
				OnDefChange(_def);
			}
		}
	}
	
	public float CritChance {
		get { return _critChance; }
		set { 
			_critChance = Mathf.Clamp(value, 0.0f, 1.0f); 

			if (OnCritChanceChange != null) {
				OnCritChanceChange(_critChance);
			}
		}
	}
	
	public float CritMultiplier {
		get { return _critMultiplier; }
		set { 
			_critMultiplier = value < 0.0f ? 0.0f : value; 

			if (OnCritMultiplierChange != null) {
				OnCritMultiplierChange(_critMultiplier);
			}
		}
	}
	
	public float SpellCritChance {
		get { return _spellCritChance; }
		set { 
			_spellCritChance = Mathf.Clamp(value, 0.0f, 1.0f); 

			if (OnSpellCritChanceChange != null) {
				OnSpellCritChanceChange(_spellCritChance);
			}
		}
	}
	
	public float SpellCritMultiplier {
		get { return _spellCritMultiplier; }
		set { 
			_spellCritMultiplier = value < 0.0f ? 0.0f : value; 

			if (OnSpellCritMultiplierChange != null) {
				OnSpellCritMultiplierChange(_spellCritMultiplier);
			}
		}
	}
	
	public int Xp {
		get { return _xp; }
		set { 
			_xp = Mathf.Clamp(value, 0, int.MaxValue); 

			if (OnXPChange != null) {
				OnXPChange(_xp);
			}
		}
	}

	public int CurrentHp {
		get { return _currentHp;}
		set { 
			_currentHp = Mathf.Clamp(value, 0, _hp); 

			if (OnCurrentHPChange != null) {
				OnCurrentHPChange(_currentHp);
			}
		}
	}

	public int CurrentMp {
		get { return _currentMp; }
		set { 
			_currentMp = Mathf.Clamp(value, 0, _mp); 

			if (OnCurrentMPChange != null) {
				OnCurrentMPChange(_currentMp);
			}
		}
	}

	public int CombosCounter {
		get { return _combosCounter; }
		set {
			_combosCounter = Mathf.Clamp(value, 0, _combosToAttack);

			if (OnCombosCounterChange != null) {
				OnCombosCounterChange(_combosCounter);
			}
		}
	}

	public int CombosToAttack {
		get { return _combosToAttack; }
		set {
			_combosToAttack = Mathf.Clamp(value, 0, int.MaxValue);

			if (OnCombosToAttackChange != null) {
				OnCombosToAttackChange(_combosToAttack);
			}
		}
	}

	public float HealRegenMultiplier {
		get { return _healRegenMultiplier; }
		set {
			_healRegenMultiplier = Mathf.Clamp(value, 0.0f, 1000.0f);
		}
	}

	public float ManaRegenMultiplier {
		get { return _manaRegenMultiplier; }
		set {
			_manaRegenMultiplier = Mathf.Clamp(value, 0.0f, 1000.0f);
		}
	}
	
	#endregion
}

