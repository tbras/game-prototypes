using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public enum HeroType { Warrior, Wizard };

public abstract class BaseHero {
	private string _name;
	private HeroStats _stats;
	private Equipment _equipment;

	// Use this for initialization
	protected BaseHero(string name, HeroStats stats, Equipment equipment) {
		_name = name;
		_stats = stats;
		_equipment = equipment;
	}
	
	public abstract HeroType GetHeroType();

	public void Attack(BaseHero target, GameBoard gameBoard) {
		var damage = CalculateDamage(
			_equipment.Weapon.MinDamage + _stats.Atk, 
			_equipment.Weapon.MaxDamage + _stats.Atk,
			_stats.CritChance, 
			_stats.CritMultiplier, 
			false);
		
		if (OnAttack != null) {
			OnAttack(ref damage, this, target, gameBoard);
		}
		
		if (OnAttacked != null) {
			OnAttacked(ref damage, target, this, gameBoard);
		}
		
		target.InflictDamage(damage, this, gameBoard, true);
	}

	public void InflictDamage(Damage incomingDamage, BaseHero attacker, GameBoard gameBoard, bool doesTriggerEvents) {
		int damageAmount = incomingDamage.Amount;

		// Spell damage go through armor
		if (incomingDamage.Type != DamageType.Spell && incomingDamage.Type != DamageType.SpellCrit) {
			damageAmount -= _stats.Def;

			if (damageAmount < 0) {
				damageAmount = 0;
			}
		}

		_stats.CurrentHp -= incomingDamage.Amount;

		// White and Spell damage events
		if (doesTriggerEvents && incomingDamage.Amount > 0) {
			if (incomingDamage.Type == DamageType.White && OnReceiveWhiteDamage != null) {
				OnDoingWhiteDamage(incomingDamage, this, attacker, gameBoard);
				OnReceiveWhiteDamage(incomingDamage, this, attacker, gameBoard);
			} else if (incomingDamage.Type == DamageType.WhiteCrit && OnReceiveWhiteCritDamage != null) {
				OnDoingWhiteCritDamage(incomingDamage, this, attacker, gameBoard);
				OnReceiveWhiteCritDamage(incomingDamage, this, attacker, gameBoard);
			} else if (incomingDamage.Type == DamageType.Spell && OnReceiveSpellDamage != null) {
				OnDoingSpellDamage(incomingDamage, this, attacker, gameBoard);
				OnReceiveSpellDamage(incomingDamage, this, attacker, gameBoard);
			} else if (incomingDamage.Type == DamageType.SpellCrit && OnReceiveSpellCritDamage != null) {
				OnDoingSpellDamage(incomingDamage, this, attacker, gameBoard);
				OnReceiveSpellDamage(incomingDamage, this, attacker, gameBoard);
			} 
		}

		if (_stats.CurrentHp == 0) {
			if (OnDead != null) {
				OnDead(incomingDamage, this, attacker, gameBoard);
			}

			if (OnKill != null) {
				OnKill(incomingDamage, attacker, this, gameBoard);
			}
		}
	} 

	public void Heal(int amount, BaseHero enemy, GameBoard gameBoard) {
		bool wasActuallyHealed = _stats.CurrentHp < _stats.HP ? true : false;

		_stats.CurrentHp += amount;

		// Trigger OnHeal just when it actually heals
		if (wasActuallyHealed) {
			if (OnHealed != null) {
				OnHealed(amount, this, enemy, gameBoard);
			}
		}
	}

	public void RegenMana(int amount, BaseHero enemy, GameBoard gameBoard) {
		bool wasManaActuallyRegened = _stats.CurrentMp < _stats.MP ? true : false;

		_stats.CurrentMp += amount;

		// Trigger OnManaRegen just when it actually heals
		if (wasManaActuallyRegened) {
			if (OnManaRegen != null) {
				OnManaRegen(amount, this, enemy, gameBoard);
			}
		}
	}

	public void StartTurn(BaseHero enemy, GameBoard gameBoard) {
		if (OnStartTurn != null) {
			OnStartTurn(this, enemy, gameBoard);
		}
	}

	public void EndTurn(BaseHero enemy, GameBoard gameBoard) {
		if (OnEndTurn != null) {
			OnEndTurn(this, enemy, gameBoard);
		}
	}

	public void TriggerOnCastSpell(BaseHero target, GameBoard gameBoard) {
		if (OnSpellCast != null) {
			OnSpellCast(this, target, gameBoard);
		}
		
		if (OnReceiveSpellCast != null) {
			OnReceiveSpellCast(target, this, gameBoard);
		}
	}

	public string Name {
		get { return _name; }
		set { _name = value; }
	}

	public HeroStats Stats {
		get { return _stats; }
	}


	#region Outgoing/Incoming Events
	
	public delegate void PreDamageDelegate(ref Damage damage, BaseHero hero, BaseHero enemy, GameBoard gameBoard);
	public event PreDamageDelegate OnAttack;
	public event PreDamageDelegate OnAttacked;

	public delegate void SpellCastDelegate(BaseHero hero, BaseHero enemy, GameBoard gameBoard);
	public event SpellCastDelegate OnSpellCast;
	public event SpellCastDelegate OnReceiveSpellCast;
	
	public delegate void PosDamageDelegate(Damage damage, BaseHero hero, BaseHero enemy, GameBoard gameBoard);
	public event PosDamageDelegate OnDoingWhiteDamage;
	public event PosDamageDelegate OnReceiveWhiteDamage;
	public event PosDamageDelegate OnDoingWhiteCritDamage;
	public event PosDamageDelegate OnReceiveWhiteCritDamage;
	public event PosDamageDelegate OnDoingSpellDamage;
	public event PosDamageDelegate OnReceiveSpellDamage;
	public event PosDamageDelegate OnDoingSpellCritDamage;
	public event PosDamageDelegate OnReceiveSpellCritDamage;
	
	public event PosDamageDelegate OnKill;	
	public event PosDamageDelegate OnDead;
	
	
	public delegate void OnHealedDelegate(int amount, BaseHero hero, BaseHero enemy, GameBoard gameBoard);
	public event OnHealedDelegate OnHealed;

	public delegate void OnManaRegenDelegate(int amount, BaseHero hero, BaseHero enemy, GameBoard gameBoard);
	public event OnManaRegenDelegate OnManaRegen;
		
	public delegate void TurnDelegate(BaseHero hero, BaseHero enemy, GameBoard gameBoard);
	public event TurnDelegate OnStartTurn;
	public event TurnDelegate OnEndTurn;

	#endregion


	#region Private Functions

	public static Damage CalculateDamage(int min, int max, float critChance, float critMultiplier, bool isSpell = false) {
		var damage = new Damage();
		
		damage.Type = UnityEngine.Random.Range(0.0f, 1.0f) <= critChance ? 
			(isSpell ? DamageType.SpellCrit : DamageType.WhiteCrit) : 
			(isSpell ? DamageType.Spell : DamageType.White);
		
		int amount = UnityEngine.Random.Range(min, max);
		
		// Multiply for its crit multiplier in case of being a critical hit
		if (damage.Type == DamageType.WhiteCrit || damage.Type == DamageType.SpellCrit) {
			amount = (int)Mathf.Round(amount * critMultiplier);
		}
		
		damage.Amount = amount;

		return damage;
	}

	#endregion
}