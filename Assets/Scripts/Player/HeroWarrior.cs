using UnityEngine;
using System.Collections;
using Weighted_Randomizer;

public class HeroWarrior : BaseHero {
	public HeroWarrior(string name, HeroStats stats, Equipment equipment) : base(name, stats, equipment) {

	}

	public override HeroType GetHeroType ()
	{
		return HeroType.Warrior;
	}
}
