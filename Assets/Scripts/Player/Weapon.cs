using UnityEngine;
using System;

public class Weapon {
	private string _name;
	private int _minDamage;
	private int _maxDamage;

	public Weapon(string name, int minDamage, int maxDamage) {
		_name = name;
		SetDamage(minDamage, maxDamage);
	}

	public void SetDamage(int min, int max) {
		_minDamage = Mathf.Clamp(min > max ? max : min, 0, int.MaxValue);
		_maxDamage = Mathf.Clamp(max < min ? min : max, 0, int.MaxValue);
	}

	public string Name {
		get { return _name; }
		set { _name = value; }
	}
	
	public int MinDamage {
		get { return _minDamage; }
	}

	public int MaxDamage {
		get { return _maxDamage; }
	}
}

