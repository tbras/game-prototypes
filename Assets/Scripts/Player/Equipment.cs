using System;

public class Equipment
{
	private Weapon _weapon;

	public Equipment(Weapon weapon) {
		_weapon = weapon;
	}

	#region Getters and Setters

	public Weapon Weapon {
		get { return _weapon; }
		set { _weapon = value; }
	}

	#endregion
}


