﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class UISpellSlot : MonoBehaviour {
	public Player caster;
	public tk2dTextMesh nameTextMesh;
	public tk2dTextMesh costTextMesh;

	private GameSceneController _gameSceneController;
	public BaseSpell spell;

	private tk2dSlicedSprite _sprite;

	// Use this for initialization
	void Start () {
		_gameSceneController = (GameSceneController)GameObject.FindWithTag(Tags.GameSceneController).GetComponent<GameSceneController>();
		_sprite = GetComponent<tk2dSlicedSprite>();

		SetVisibility(false);

		nameTextMesh.color = new Color(27f / 80f, 19f / 63f, 13 / 42f);

		if (spell != null) {
			nameTextMesh.text = spell.name;
			costTextMesh.text = spell.manaCost.ToString();
		}
		
		SetVisibility(true);

		StartCoroutine(coInput());
	}
	
	// Update is called once per frame
	void Update () {
		if (IsCastable()) {
			SetAlpha(1.0f);
		} else {
			SetAlpha(0.6f);
		}
	}

	private IEnumerator coInput() {
		while (true) {
			if (Input.GetMouseButtonUp(0)) {
				Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
				RaycastHit hit;

				Debug.Log("IsCastable() = " + IsCastable().ToString());

				if (collider.Raycast(ray, out hit, 100000f) && IsCastable()) {
					Debug.Log("Casting spell");

					yield return StartCoroutine(spell.coCast(caster, _gameSceneController.GetPlayerOpponent(caster), _gameSceneController.gameBoard));
				}
			}

			yield return null;
		}
	}

//	public void SetSpell(BaseSpell spell) {
//		_spell = spell;
//
//		nameTextMesh.text = spell.name;
//		costTextMesh.text = spell.manaCost.ToString();
//
//		SetVisibility(true);
//	}

	private bool IsCastable() {
		if (caster == null || spell == null ) {
			return false;
		}

		if (_gameSceneController.CurrentPlayer == caster && spell.manaCost <= caster.Hero.Stats.CurrentMp) {
			return true;
		} else {
			return false;
		}
	}

	public void SetVisibility(bool visibility) {
		if (spell == null) {
			visibility = false;
		}

		renderer.enabled = visibility;
		nameTextMesh.renderer.enabled = visibility;
		costTextMesh.renderer.enabled = visibility;
	}
	

	private void SetAlpha(float alpha) {
		Color color = Color.white;
		color.a = alpha;
		_sprite.color = color;

		color = nameTextMesh.color;
		color.a = alpha;
		nameTextMesh.color = color;

		color = costTextMesh.color;
		color.a = alpha;
		costTextMesh.color = color;
	}
}
