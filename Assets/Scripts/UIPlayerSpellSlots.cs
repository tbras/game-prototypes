﻿using UnityEngine;
using System.Collections;

public class UIPlayerSpellSlots : MonoBehaviour {
	public UISpellSlot spellSlot1;
	public UISpellSlot spellSlot2;
	public UISpellSlot spellSlot3;
	public UISpellSlot spellSlot4;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SetVisibility(bool visibility) {
		if (spellSlot1 != null) {
			spellSlot1.SetVisibility(visibility);
		}

		if (spellSlot2 != null) {
			spellSlot2.SetVisibility(visibility);
		}

		if (spellSlot3 != null) {
			spellSlot3.SetVisibility(visibility);
		}

		if (spellSlot4 != null) {
			spellSlot4.SetVisibility(visibility);
		}
	}
}
