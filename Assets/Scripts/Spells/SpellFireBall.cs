﻿using System.Collections;
using UnityEngine;

public class SpellFireBall : BaseSpell {
	public int minDamage = 2;
	public int maxDamage = 4;

	public Transform fxPrefab;

	public Transform spellFXEndPosition;

	void Awake() {
		maxDamage = maxDamage < minDamage ? minDamage : maxDamage;
	}

	void Start() {
		ObjectPool.CreatePool(fxPrefab);
	}
	
	#region Public API
	
	public override IEnumerator coCast(Player caster, Player opponent, GameBoard gameBoard) {
		caster.Hero.Stats.CurrentMp -= manaCost;
		caster.Hero.TriggerOnCastSpell(opponent.Hero, gameBoard);

		Damage damage = BaseHero.CalculateDamage(
			minDamage, maxDamage, 
			caster.Hero.Stats.SpellCritChance, 
			caster.Hero.Stats.SpellCritMultiplier, 
			true);

		Debug.Log("Cast");

		Transform p = fxPrefab.Spawn(opponent.transform.Find("Avatar").transform.position);

		yield return new WaitForSeconds(2.0f);

		opponent.Hero.InflictDamage(damage, caster.Hero, gameBoard, true);

		yield return new WaitForSeconds(5.0f);

		p.Recycle();



		yield return null;
	}

	#endregion
}
