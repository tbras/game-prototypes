﻿using System.Collections;
using UnityEngine;

public abstract class BaseSpell : MonoBehaviour {
	public string name;
	public int manaCost;
	public int cooldown;

	#region Public API

	public abstract IEnumerator coCast(Player caster, Player opponent, GameBoard gameBoard); /* {
		caster.Hero.Stats.CurrentMp -= manaCost;
		caster.Hero.TriggerOnCastSpell(opponent.Hero, gameBoard);

		yield return StartCoroutine(coOnCast(caster, opponent, gameBoard));
	}
	*/

//	public abstract IEnumerator coOnCast(Player caster, Player opponent, GameBoard gameBoard);

	#endregion
}

