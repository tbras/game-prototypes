﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class HeroBrain {
	private BaseHero _hero;

	public HeroBrain(BaseHero hero) {
		_hero = hero;

	}
	
	public IEnumerator coPlay(GameSceneController gameSceneController) {
		yield return new WaitForSeconds(UnityEngine.Random.Range(0.2f, 0.5f));

		GameBoard gameBoard = gameSceneController.gameBoard;

		BoardPattern[][] patternsOrdered = new BoardPattern[][]{
			BoardPattern.All5ComboPatterns,
			BoardPattern.All4ComboPatterns,
			BoardPattern.All3ComboPatterns
		};

		BlockType[] blockTypesOrdered = new BlockType[]{
			BlockType.Sword,
			BlockType.Shield,
			BlockType.Experience,
			BlockType.Health,
			BlockType.Mana,
			BlockType.Gold
		};

		for (int i = 0; i < patternsOrdered.Length; i++) {
			var moves = gameBoard.GetPossibleMovesForPatterns(patternsOrdered[i]);

			for (int j = 0; j < blockTypesOrdered.Length; j++) {
				var m = FilterMovesForType(moves, blockTypesOrdered[j], gameSceneController);

				if (m != null) {
					yield return gameSceneController.StartCoroutine(gameBoard.coSwapBlocks(m[0].FromPosition, m[0].ToPosition));

					yield break;
				}
			}
		}

		Debug.Log("Should not happen");

		// This SHOULD NOT HAPPEN
		var backupMoves = gameBoard.GetPossibleMoves(true);

		if (backupMoves.Count < 1) {
			throw new UnityException("SHOULD NOT HAPPEN!!! EVER!!!");
		}
		
		yield return gameSceneController.StartCoroutine(gameBoard.coSwapBlocks(backupMoves[0].FromPosition, backupMoves[0].ToPosition));
	} 

	public List<BoardMove> FilterMovesForType(List<BoardMove> moves, BlockType blockType, GameSceneController gameSceneController) {
		List<BoardMove> movesFiltered = new List<BoardMove>();

		for (int i = 0; i < moves.Count; i++) {
			if (gameSceneController.gameBoard.BlockAt(moves[i].FromPosition).blockType == blockType) {
				movesFiltered.Add(moves[i]);
			}
		}

		return movesFiltered.Count > 0 ? movesFiltered : null;
	}
}
