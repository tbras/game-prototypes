﻿using UnityEngine;
using System.Collections;

public class PrefabsManager : MonoBehaviour {
	public BlocksPrefabs blocksPrefabs;
	public SpellsPrefabs spellsPrefabs;

	private static PrefabsManager _instance;
	
	//This is the public reference that other classes will use
	public static PrefabsManager instance {
		get {
			if(_instance == null)
				_instance = GameObject.FindObjectOfType<PrefabsManager>();
			return _instance;
		}
	}
}


