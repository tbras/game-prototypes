﻿using UnityEngine;
using System.Collections;

public class BlocksPrefabs : MonoBehaviour {
	public GameBlock swordPrefab;
	public GameBlock shieldPrefab;
	public GameBlock healthPrefab;
	public GameBlock manaPrefab;
	public GameBlock experiencePrefab;
	public GameBlock goldPrefab;

	// Use this for initialization
	void Start () {
		// Create a pool for each block prefab
		ObjectPool.CreatePool(swordPrefab);
		ObjectPool.CreatePool(shieldPrefab);
		ObjectPool.CreatePool(healthPrefab);
		ObjectPool.CreatePool(manaPrefab);
		ObjectPool.CreatePool(experiencePrefab);
		ObjectPool.CreatePool(goldPrefab);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
