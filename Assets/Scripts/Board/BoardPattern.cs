using System;
using System.Collections.Generic;

public class BoardPattern {
	#region Static Patterns

	// X
	// X
	// OX
	public static readonly BoardPattern Pattern3Combo1 = new BoardPattern(new int[,]{{0, 0}, {1, 0}, {2, 1}}, new int[,]{{2, 1}, {2, 0}});

	//  X
	//  X
	// XO
	public static readonly BoardPattern Pattern3Combo2 = new BoardPattern(new int[,]{{0, 1}, {1, 1}, {2, 0}}, new int[,]{{2, 0}, {2, 1}});

	// X
	// OX
	// X
	public static readonly BoardPattern Pattern3Combo3 = new BoardPattern(new int[,]{{0, 0}, {1, 1}, {2, 0}}, new int[,]{{1, 1}, {1, 0}});

	// X
	// X
	// O
	// X
	public static readonly BoardPattern Pattern3Combo4 = new BoardPattern(new int[,]{{0, 0}, {1, 0}, {3, 0}}, new int[,]{{3, 0}, {2, 0}});

	// X
	// OX
	// X
	// X
	public static readonly BoardPattern Pattern4Combo1 = new BoardPattern(new int[,]{{0, 0}, {1, 1}, {2, 0}, {3, 0}}, new int[,]{{1, 1}, {1, 0}});

	// X
	// X
	// OX
	// X
	public static readonly BoardPattern Pattern4Combo2 = new BoardPattern(new int[,]{{0, 0}, {1, 0}, {2, 1}, {3, 0}}, new int[,]{{2, 1}, {2, 0}});

	//   X
	// XXO
	//   X
	//   X
	public static readonly BoardPattern Pattern5Combo1 = new BoardPattern(new int[,]{{0, 2}, {1, 0}, {1, 2}, {2, 2}, {2, 2}}, new int[,]{{0, 2}, {1, 2}});

	// XXOX
	//   X
	//   X
	public static readonly BoardPattern Pattern5Combo2 = new BoardPattern(new int[,]{{0, 0}, {0, 1}, {0, 3}, {1, 2}, {2, 2}}, new int[,]{{0, 3}, {0, 2}});

	// X
	// X
	// OX
	// X
	// X
	public static readonly BoardPattern Pattern5Combo3 = new BoardPattern(new int[,]{{0, 0}, {1, 0}, {2, 1}, {3, 0}, {4, 0}}, new int[,]{{2, 1}, {2, 0}});

	public static readonly BoardPattern[] All3ComboPatterns = new BoardPattern[]{
		BoardPattern.Pattern3Combo4,
		BoardPattern.Pattern3Combo3,
		BoardPattern.Pattern3Combo2,
		BoardPattern.Pattern3Combo1,
	};

	public static readonly BoardPattern[] All4ComboPatterns = new BoardPattern[]{
		BoardPattern.Pattern4Combo2,
		BoardPattern.Pattern4Combo1,
	};

	public static readonly BoardPattern[] All5ComboPatterns = new BoardPattern[]{
		BoardPattern.Pattern5Combo3,
		BoardPattern.Pattern5Combo2,
		BoardPattern.Pattern5Combo1,
	};

	public static readonly BoardPattern[] AllPatterns = new BoardPattern[]{
		BoardPattern.Pattern5Combo3,
		BoardPattern.Pattern5Combo2,
		BoardPattern.Pattern5Combo1,
		BoardPattern.Pattern4Combo2,
		BoardPattern.Pattern4Combo1,
		BoardPattern.Pattern3Combo4,
		BoardPattern.Pattern3Combo3,
		BoardPattern.Pattern3Combo2,
		BoardPattern.Pattern3Combo1,
	};

	#endregion

	#region Private Fields

	private BoardPosition[] _pattern;
	private BoardMove _move;
	private int _width;
	private int _height;

	#endregion

	#region Constructors

	public BoardPattern(BoardPosition[] pattern, BoardMove move, int rotateRight = 0) {
		int[] minMaxRow = new int[]{ MinRow(pattern), MaxRow(pattern) };
		int[] minMaxColumn = new int[]{ MinColumn(pattern), MaxColumn(pattern) };
		
		if (move.FromPosition.row < minMaxRow[0] || 
		    move.FromPosition.row > minMaxRow[1] ||
		    move.FromPosition.column < minMaxColumn[0] ||
		    move.FromPosition.column > minMaxColumn[1]) {
			throw new Exception("BoardMove.FromPosition '" + move.FromPosition.ToString() + "' is not inside pattern boundaries.");
		}
		
		if (move.ToPosition.row < minMaxRow[0] || 
		    move.ToPosition.row > minMaxRow[1] ||
		    move.ToPosition.column < minMaxColumn[0] ||
		    move.ToPosition.column > minMaxColumn[1]) {
			throw new Exception("BoardMove.ToPosition '" + move.ToPosition.ToString() + "' is not inside pattern boundaries.");
		}

		// FromPosition must be one of the pattern positions
		bool valid = false;
		for (int i = 0; i < pattern.Length; i++) {
			if (pattern[i] == move.FromPosition) {
				valid = true;
			}
		}

		if (!valid) {
			throw new Exception("BoardMove.FromPosition '" + move.FromPosition.ToString() + "' is not one of the pattern's positions.");
		}

		_pattern = pattern;
		_move = move;
		_width = minMaxColumn[1] - minMaxColumn[0] + 1;
		_height = minMaxRow[1] - minMaxRow[0] + 1;

		for (int i = 0; i < rotateRight; i++) {
			RotateRight();
		}
	}

	public BoardPattern(int[,] intPairArrayPattern, int[,] move, int rotateRight = 0) : 
	this(BoardPattern.IntPairArrayToBoardPositionArray(intPairArrayPattern), BoardPattern.IntPairArrayToBoardMove(move), rotateRight) {

	}

	#endregion

	#region Getters and Setters

	public BoardPosition[] Pattern {
		get { return _pattern; }
	}

	public BoardMove Move {
		get {return _move; }
	}

	public int Width {
		get { return _width; }
	}

	public int Height {
		get { return _height; }
	}

	#endregion

	#region Public Functions

	public void RotateRight() {
		int tmp;

		// Rotate pattern
		for (int i = 0; i < _pattern.Length; i++) {
			tmp = _pattern[i].column;

			_pattern[i].column = _height - _pattern[i].row - 1;
			_pattern[i].row = tmp;
		}

		// Rotate FromPosition
		tmp = _move.FromPosition.column;
		
		_move.FromPosition.column = _height - _move.FromPosition.row - 1;
		_move.FromPosition.row = tmp;

		// Rotate ToPosition
		tmp = _move.ToPosition.column;
		
		_move.ToPosition.column = _height - _move.ToPosition.row - 1;
		_move.ToPosition.row = tmp;

		// Update Width and Height
		tmp = _width;

		_width = _height;
		_height = tmp;
	}

	#endregion

	#region Private Functions

	private int MinRow(BoardPosition[] positions) {
		if (positions == null)
			throw new System.ArgumentNullException ("positions");
		
		int min = positions[0].row; 
		
		for (int i = 1; i < positions.Length; i++) {
			if (positions[i].row < min)
				min = positions[i].row;
		}
		
		return min;
	}
	
	private int MinColumn(BoardPosition[] positions) {
		if (positions == null)
			throw new System.ArgumentNullException ("positions");
		
		int min = positions[0].column;
		
		for (int i = 1; i < positions.Length; i++) {
			if (positions[i].column < min)
				min = positions[i].column;
		}
		
		return min;
	}
	
	private int MaxRow(BoardPosition[] positions) {
		if (positions == null)
			throw new System.ArgumentNullException ("positions");
		
		int max = positions[0].row;
		
		for (int i = 1; i < positions.Length; i++) {
			if (positions[i].row > max)
				max = positions[i].row;
		}
		
		return max;
	}
	
	private int MaxColumn(BoardPosition[] positions) {
		if (positions == null)
			throw new System.ArgumentNullException ("positions");
		
		int max = positions[0].column;
		
		for (int i = 1; i < positions.Length; i++) {
			if (positions[i].column > max)
				max = positions[i].column;
		}
		
		return max;
	}

	#endregion

	#region Static Functions

	public static BoardPosition[] IntPairArrayToBoardPositionArray(int[,] array) {
		BoardPosition[] posArray = new BoardPosition[array.GetLength(0)];

		for (int i = 0; i < array.GetLength(0); i++) {
			posArray[i] = new BoardPosition(array[i, 0], array[i, 1]);
		}

		return posArray;
	}

	public static BoardMove IntPairArrayToBoardMove(int[,] boardMove) {
		return new BoardMove(new BoardPosition(boardMove[0, 0], boardMove[0, 1]), 
		                     new BoardPosition(boardMove[1, 0], boardMove[1, 1]));
	}

	#endregion
}


