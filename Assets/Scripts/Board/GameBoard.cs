using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Weighted_Randomizer;

public enum BoardState { NotInit, Animating, Unchecked, Checking, Checked };

public class GameBoard : MonoBehaviour {
	public enum MovementDirection { Up, Down, Right, Left };

	#region Private Fields

	private const int ROWS = 7;
	private const int COLUMNS = 7;
	
	private Vector2 _boardSize;
	private float _blockSize;

	private GameBlock[,] _board;
	private List<GameBlock> _blocks;
	private BoardState _state;
	private BlockGenerator _blockGenerator;
	private bool _swappingBlocks = false;
	private bool _isUserInteractionEnabled = true;
	private int _comboTier = 1;	// It means how many combos in a row have happened so far

	#endregion

	#region Delegates / Events
	
	public delegate void OnBlockSelectionDelegate(BoardPosition position);
	public delegate IEnumerator coOnCombosDelegate(List<Combo> combos);
	public delegate void OnSwappingBlocksCombosCheckedDelegate();
	
	public event OnBlockSelectionDelegate OnBlockSelection;
	public event coOnCombosDelegate coOnCombosPreGravity;
	public event coOnCombosDelegate coOnCombosPosGravity;
	public event OnSwappingBlocksCombosCheckedDelegate OnSwappingBlocksCombosChecked;
	
	#endregion

	#region Public Fields
	public Transform selectedBlock;

	[Range(0.1f, 1.0f)] 
	public float movementDectectionCoefficient = 0.7f;
	[Range(3, 4)] 
	public int matchNumber = 3; 
	public float smoothing = 30f;

	public FloatingText floatText;

	#endregion

	#region Awake/Start/Update
	
	void Awake() {
		_board = new GameBlock[ROWS, COLUMNS];
		_blocks = new List<GameBlock>();
		_state = BoardState.NotInit;
	}
	
	// Use this for initialization
	void Start () {
		_blockGenerator = new BlockGenerator();
		_boardSize = GetComponent<MeshFilter>().mesh.bounds.size;
		_blockSize = Mathf.Floor(_boardSize.x / COLUMNS);
		
		selectedBlock = (Transform)Instantiate(selectedBlock, Vector3.zero, Quaternion.identity);
		selectedBlock.parent = transform;
		selectedBlock.renderer.enabled = false;

		StartCoroutine(coManageInput());
	}
	
	// Update is called once per frame
	void Update () {
		if (_state == BoardState.Unchecked) {
			StartCoroutine(coCheckBoard());
		} else if (_state == BoardState.NotInit) {
			StartCoroutine(coFillBoard());
		}
	}

	#endregion

	#region Public API

	public void EnableUserInteration() {
		_isUserInteractionEnabled = true;
	}

	public void DisableUserInteration() {
		_isUserInteractionEnabled = false;
	}

	public GameBlock BlockAt(int row, int column) {
		return _board[row, column];
	}
	
	public GameBlock BlockAt(BoardPosition position) {
		return BlockAt(position.row, position.column);
	}
	
	public void SetBlockAt(GameBlock block, int row, int column) {
		if (row < 0 || row >= ROWS || column < 0 || column >= COLUMNS) {
			Debug.Log("Invalid SetBlockAt position: (" + row + ", " + ")");
		}
		
		_board[row, column] = block;
	}
	
	public void SetBlockAt(GameBlock block, BoardPosition position) {
		SetBlockAt(block, position.row, position.column);
	}
	
	public void RemoveBlockAt(int row, int column) {
		if (BlockAt(row, column) == null) {
			Debug.Log("Block at (" + row + ", " + column + ") = null");
			return;
		}
		
		_blocks.Remove(BlockAt(row, column));
		BlockAt(row, column).Dispose();
		SetBlockAt(null, row, column);
	}
	
	public void RemoveBlockAt(BoardPosition position) {
		RemoveBlockAt(position.row, position.column);
	}
	
	public void RemoveBlocksAt(List<BoardPosition> positions) {
		for(int i = 0; i < positions.Count; i++) {
			RemoveBlockAt(positions[i]);
		}
	}

	public void ReplaceRandomBlockByRandomBlock() {
//		int column = Random.Range(0, COLUMNS - 1);
//		int row = Random.Range(0, ROWS - 1);
//
//		RemoveBlockAt(row, column);
//		SetBlockAt(CreateBlock(BoardPositionToLocal(row, column)), row, column);
//
//		_state = BoardState.Unchecked;
	}

	public void ResetBoard() {
		// Fill the board
		for (int r = 0; r < ROWS; r++) {
			for (int c = 0; c < COLUMNS; c++) {
				if (BlockAt(r, c) != null) {
					RemoveBlockAt(r, c);
				}
				
				SetBlockAt(CreateBlock(), r, c);
				BlockAt(r, c).LocalPosition = BoardPositionToLocal(r, c);
			}
		}
	}

	public bool IsSwapEnable() {
		return _state == BoardState.Checked;
	}
	
	#endregion

	#region Input
	private IEnumerator coManageInput() {
		bool isButtonDown = false;
		bool hasDetectedMovement = false;
		Vector3 firstMousePosition = Vector3.zero;
		
		while (true) {
			// Disable input if it's animating and that
			if(_isUserInteractionEnabled && (_state == BoardState.Checked)) {
				// Only if a block isn't selected yet
				if (!isButtonDown) {
					if (Input.GetMouseButtonDown (0)) {
						Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
						RaycastHit hit;
						
						if (Physics.Raycast(ray, out hit)) {
							BoardPosition boardPos = WorldPointToBoard(hit.point);
							
							isButtonDown = true;
							firstMousePosition = hit.point;
						} else {
							UnselectBlock();
						}
					}
				} else {
					// Only if a movement hasn't been completed
					if (!hasDetectedMovement) {
						RaycastHit hit;

						if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit)) {
							if (Vector3.Distance(firstMousePosition, hit.point) > _blockSize * movementDectectionCoefficient) {
								UnselectBlock();
								
								var fromPos = WorldPointToBoard(firstMousePosition);
								var toPos = new BoardPosition();

								var direction = GetMovementDirection(firstMousePosition, hit.point);

								if (direction == MovementDirection.Left && fromPos.column > 0) {
									toPos.SetPosition(fromPos.row, fromPos.column-1);
								} else if (direction == MovementDirection.Right && fromPos.column < COLUMNS - 1) {
									toPos.SetPosition(fromPos.row, fromPos.column+1);
								} else if (direction == MovementDirection.Up && fromPos.row > 0) {
									toPos.SetPosition(fromPos.row-1, fromPos.column);
								} else {
									toPos.SetPosition(fromPos.row+1, fromPos.column);
								}

								StartCoroutine(coSwapBlocks(fromPos, toPos));

								hasDetectedMovement = true;
							}
						}
					}
				}
			}
			
			
			// Reset variables on mouse boutton up
			if (Input.GetMouseButtonUp(0)) {
				isButtonDown = false;
				hasDetectedMovement = false;

				if (OnBlockSelection != null) {
					OnBlockSelection(WorldPointToBoard(firstMousePosition));
				}
			}
			
			yield return null;
		}
	}
	
	#endregion
	
	#region Routines

	private IEnumerator coFillBoard() {
		// Fill the board
		for (int r = 0; r < ROWS; r++) {
			for (int c = 0; c < COLUMNS; c++) {
				SetBlockAt(CreateBlock(), r, c);
			}
		}
		
		int counter = 0;
		
		// Keep removing combos and adding new blocks until there are any combos
		do {
			// Just in case there's a non stop insanely stupid combination
			if (counter > 10) {
				ResetBoard();
				
				counter = 0;
			}
			
			var combos = CheckForCombos(); 
			
			// Remove any combos
			for (int i = 0; i < combos.Count; i++) {
				RemoveBlocksAt(combos[i].Positions);
			}
			
			counter++;
		}while (ApplyGravity(false));
		
		// Update blocks' Position and TargetPosition
		for (int r = 0; r < ROWS; r++) {
			for (int c = 0; c < COLUMNS; c++) {
				var targetPosition = BoardPositionToLocal(r, c);
				var initialPosition = new Vector3(targetPosition.x, targetPosition.y + (_blockSize * 3 * c) + _boardSize.y * 0.5f, targetPosition.z);
				
				BlockAt(r, c).LocalPosition = initialPosition;
				StartCoroutine(BlockAt(r, c).coMoveTo(targetPosition));
			}
		}
		
		_state = BoardState.Animating;
		
		yield return StartCoroutine(coWaitUntilBlocksStopMoving());
		
		_state = BoardState.Unchecked;
	}
	
	public IEnumerator coSwapBlocks(BoardPosition boardPositionA, BoardPosition boardPositionB, bool matchRequired = true, bool isMainPlayerMove = true) {
		_swappingBlocks = isMainPlayerMove;

		GameBlock blockA = BlockAt(boardPositionA);
		GameBlock blockB = BlockAt(boardPositionB);
		Vector3 blockAPosition = blockA.LocalPosition;
		Vector3 blockBPosition = blockB.LocalPosition;
		
		StartCoroutine(blockA.coMoveTo(blockBPosition));
		StartCoroutine(blockB.coMoveTo(blockAPosition));
		
		_state = BoardState.Animating;

		SetBlockAt(blockA, boardPositionB);
		SetBlockAt(blockB, boardPositionA);
		
		yield return StartCoroutine(coWaitUntilBlocksStopMoving());
		
		if (CheckForCombos().Count > 0) {
			_state = BoardState.Unchecked;
		} else {
			if (matchRequired) {
				yield return new WaitForSeconds(0.02f);

				StartCoroutine(blockA.coShakeHorizontally(2, 0.2f, true));
				StartCoroutine(blockB.coShakeHorizontally(2, 0.2f, false));

				yield return StartCoroutine(coWaitUntilBlocksStopMoving());

				yield return StartCoroutine(coSwapBlocks(boardPositionA, boardPositionB, false));
			}

			_state = BoardState.Checked;
			_swappingBlocks = false;
		}
	}

	private IEnumerator coCheckBoard() {      
		_state = BoardState.Checking;
		
		List<Combo> combos = CheckForCombos();
		
		// Check for matches
		if (combos.Count > 0) {
			for (int i = 0; i < combos.Count; i++) {
				combos[i].Tier = _comboTier;	// Set combo tier
				
				for (int j = 0; j < combos[i].Positions.Count; j++) {
					RemoveBlockAt(combos[i].Positions[j]);
				}
			}

			_comboTier++;	// The next combos will be tier + 1
		} else {
			_state = BoardState.Checked;

			if (_swappingBlocks) {
				OnSwappingBlocksCombosChecked();

				_swappingBlocks = false;
			}

			_comboTier = 1;	// Reset combo tier
		}

		if (coOnCombosPreGravity != null && combos.Count > 0) {
			yield return StartCoroutine(coOnCombosPreGravity(combos));
		}
		
		if (ApplyGravity(true)) {
			_state = BoardState.Animating;
			
			yield return StartCoroutine(coWaitUntilBlocksStopMoving());

			if (coOnCombosPosGravity != null && combos.Count > 0) {
				yield return StartCoroutine(coOnCombosPosGravity(combos));
			}

			_state = BoardState.Unchecked;
		}

		// Check if there are possible moves
		if (GetPossibleMoves(true).Count == 0) {
			ResetBoard();
//			EmptyBoard();

//			yield return new WaitForSeconds(1.0f);

//			yield return StartCoroutine(coFillBoard());
			
			_state = BoardState.Unchecked;
		}
	}
	
	private IEnumerator coWaitUntilBlocksStopMoving() {
		while (AreBlocksMoving()) {
			yield return null;
		}
	}
	
	#endregion

	#region Helpers

	/// <summary>
	/// Applies gravity to the game board, ie, existing blocks will fall
	/// through empty blocks. New blocks are also created.
	/// </summary>
	/// /// <param name="prepareBlocksForAnimation">If set to <c>true</c> blocks's Position and TargetPosition are set.</param>
	/// <returns><c>true</c>, if any block has changed its position, <c>false</c> otherwise.</returns>
	private bool ApplyGravity(bool prepareBlocksForAnimation = false) {
		var existingBlocks = new List<GameBlock>();
		var newBlocks = new List<GameBlock>();
		
		bool boardChanged = false;
		
		// Refill empty spots
		for (int c = 0; c < COLUMNS; c++) {
			existingBlocks.Clear();
			newBlocks.Clear();

			// Separate new blocks from existing ones
			for (int r = ROWS-1; r >= 0; r--) {
				var currBlock = BlockAt(r, c);

				if (currBlock == null) {
					var b = CreateBlock();
					b.transform.position = BoardPositionToLocal(-1 - newBlocks.Count, c);
				
					newBlocks.Add(b);
				} else {
					existingBlocks.Add(currBlock);
				}
			}
			
			// Set new target
			if (newBlocks.Count > 0) {
				existingBlocks.AddRange(newBlocks);
				
				for (int i = 0; i < existingBlocks.Count; i++) {
					if (prepareBlocksForAnimation) {
						StartCoroutine(existingBlocks[i].coMoveTo(BoardPositionToLocal(ROWS-i-1, c)));
					} else {
						existingBlocks[i].transform.position = BoardPositionToLocal(ROWS-i-1, c);
					}
					
					SetBlockAt(existingBlocks[i], ROWS-i-1, c);
				}
				
				boardChanged = true;
			}
		}

		return boardChanged;
	}
	
	private bool AreBlocksMoving() {
		for (int i = 0; i < _blocks.Count; i++) {
			if (_blocks[i].IsAnimating) {
				return true;
			}
		}
		
		return false;
	}
	
	private GameBlock CreateBlock() {
		GameBlock block = _blockGenerator.NextBlock();

		block.transform.parent = transform;
		_blocks.Add(block);
		
		return block;
	}

	private GameBlock CreateBlock(Vector3 position) {
		GameBlock gameBlock = CreateBlock();
		gameBlock.transform.position = position;

		return gameBlock;
	}

	private void EmptyBoard() {
		for (int r = 0; r < ROWS; r++) {
			for (int c = 0; c < COLUMNS; c++) {
				RemoveBlockAt(r, c);
			}
		}
	}

	private BoardPosition WorldPointToBoard(Vector3 point) {
		// Fix board origin
		point.x -= transform.position.x;
		point.y -= transform.position.y;
		
		BoardPosition boardPos = new BoardPosition();
		boardPos.row = (int)Mathf.Clamp(ROWS - 1 - Mathf.Floor((point.y + (_boardSize.y / 2)) * COLUMNS / _boardSize.y), 0, ROWS - 1);
		boardPos.column = (int)Mathf.Clamp(Mathf.Floor((point.x + (_boardSize.x / 2)) * COLUMNS / _boardSize.x), 0, COLUMNS - 1);
		
		return boardPos;
	}
	
	private Vector3 BoardPositionToLocal(int row, int column) {
		float x = Mathf.Floor((-_blockSize * COLUMNS * 0.5f) + (_blockSize * column) + (_blockSize * 0.5f));
		float y = Mathf.Floor((_blockSize * ROWS * 0.5f) - (_blockSize * row) - (_blockSize * 0.5f));
		
		Vector3 localPoint = new Vector3(x, y, 0f);
//		localPoint.x += transform.position.x;
//		localPoint.y += transform.position.y;
		
		return localPoint;
	}
	
	private Vector3 BoardPositionToLocal(BoardPosition boardPosition) {
		return BoardPositionToLocal(boardPosition.row, boardPosition.column);
	}

	private void SelectBlock(BoardPosition boardPosition) {
		selectedBlock.position = BoardPositionToLocal(boardPosition);
		selectedBlock.renderer.enabled = true;
	}
	
	private void UnselectBlock() {
		selectedBlock.renderer.enabled = false;
	}

	private BoardPosition[] Row(int row) {
		BoardPosition[] boardRow = new BoardPosition[COLUMNS];
		
		for (int c = 0; c < COLUMNS; c++) {
			boardRow[c] = new BoardPosition(row, c);
		}
		
		return boardRow;
	}
	
	private BoardPosition[] Column(int column) {
		BoardPosition[] boardColumn = new BoardPosition[ROWS];
		
		for (int r = 0; r < ROWS; r++) {
			boardColumn[r] = new BoardPosition(r, column);
		}
		
		return boardColumn;
	}
	
	private MovementDirection GetMovementDirection(Vector3 start, Vector3 end) {
		Vector3 movementDirection = start - end;
		
		// Check movement's direction
		if (Mathf.Abs(movementDirection.x) >= Mathf.Abs(movementDirection.y)) {
			// Check if the movement went right or left
			if (movementDirection.x < 0) {
				return MovementDirection.Right;
			} else {
				return MovementDirection.Left;
			}
		} else {
			if (movementDirection.y < 0) {
				return MovementDirection.Up;
			} else {
				return MovementDirection.Down;
			}
		}
	}
	
	#endregion
	
	#region Combos
	
	private List<Combo> CheckForCombos() {
		var combos = new List<Combo>();

		// Search for horizontal combos
		for(int r = 0; r < ROWS; r++) {
			var m = CheckForCombosInArray(Row(r));
			
			if (m.Count > 0) {
				combos.AddRange(m);
			}
		}

		// Search for vertical combos
		for(int c = 0; c < COLUMNS; c++) {
			var m = CheckForCombosInArray(Column(c));
			
			if (m.Count > 0) {
				combos.AddRange(m);
			}
		}

		// Merge any combos that have blocks in common
		for (int i = 0; i < combos.Count; i++) {
			for (int j = 0; j < combos.Count; j++) {
				if ((i != j) && combos[i].HasAnyPositionInCommon(combos[j])) {
					combos[i].AppendMatchPositions(combos[j]);
					combos.RemoveAt(j);
					
					// Reset loop
					j = combos.Count;
					i = -1;
				}
			}
		}
		
		return combos;
	}
	
	private List<Combo> CheckForCombosInArray(BoardPosition[] array) {
		var combos = new List<Combo>();
		int comboCount = 1;
		for (int i = 1; i < array.Length; i++) {
			GameBlock last = BlockAt(array[i-1]);
			GameBlock current = BlockAt(array[i]);
			
			if (current.IsMatch(last)) {
				comboCount++;
			} else {
				if (comboCount >= matchNumber) {
					var combo = new Combo();
					for (int counter = 0; counter < comboCount; counter++) {
						combo.Add(new BoardPosition(array[i-1-counter].row, array[i-1-counter].column));
					}
					combo.Type = BlockAt(combo.Positions[0]).blockType;
					combos.Add(combo);
				}
				
				comboCount = 1;
			}
		}

		// Last combo (end of array)
		if (comboCount >= matchNumber) {
			var combo = new Combo();
			for (int counter = 0; counter < comboCount; counter++) {
				combo.Add(new BoardPosition(array[array.Length-1-counter].row, array[array.Length-1-counter].column));
			}
			combo.Type = BlockAt(combo.Positions[0]).blockType;
			combos.Add(combo);
		}
		
		return combos;
	}

	#endregion

	#region Moves (Public API)
	
	public List<BoardMove> GetPossibleMoves(bool returnOnFirstMove = false) {
		return GetPossibleMovesForPatterns(BoardPattern.AllPatterns, returnOnFirstMove);
	}
	
	public List<BoardMove> GetPossibleMovesForPattern(BoardPattern pattern, bool returnOnFirstMove = false) {
		var moves = new List<BoardMove>();
		
		var lastPos = new BoardPosition();
		var currPos = new BoardPosition();

		for (int r = 0; r <= ROWS-pattern.Height; r++) {
			for (int c = 0; c <= COLUMNS-pattern.Width; c++) {
				var positions = pattern.Pattern;
				
				// Check if there are a match
				bool isMatch = true;
				for (int i = 1; i < positions.Length; i++) {
					lastPos.row = positions[i-1].row + r;
					lastPos.column = positions[i-1].column + c;
					currPos.row = positions[i].row + r;
					currPos.column = positions[i].column + c;
					
					if (!BlockAt(lastPos).IsMatch(BlockAt(currPos))) {
						isMatch = false;
						
						break;
					}
				}
				
				if (isMatch) {
					var fromPos = new BoardPosition();
					var toPos = new BoardPosition();
					
					fromPos.row = pattern.Move.FromPosition.row + r;
					fromPos.column = pattern.Move.FromPosition.column + c;
					toPos.row = pattern.Move.ToPosition.row + r;
					toPos.column = pattern.Move.ToPosition.column + c;
					
					moves.Add(new BoardMove(fromPos, toPos));
					
					if (returnOnFirstMove) {
						return moves;
					}
				}
			}
		}
		
		return moves;
	}

	public List<BoardMove> GetPossibleMovesForPatterns(BoardPattern[] boardPatterns, bool returnOnFirstMove = false) {
		var moves = new List<BoardMove>();
		var patterns = boardPatterns;
		
		for (int i = 0; i < patterns.Length; i++) {
			// Rotations
			for (int j = 0; j < 3; j++) {
				var m = GetPossibleMovesForPattern(patterns[i], returnOnFirstMove);
				
				if (m.Count > 0) {
					moves.AddRange(m);
					
					if (returnOnFirstMove) {
						return moves;
					}
				}
				
				patterns[i].RotateRight();
			}
		}
		
		return moves;
	}
	
	#endregion
}
