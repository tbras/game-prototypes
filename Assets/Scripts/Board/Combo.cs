using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Combo {
	private BlockType _blockType;
	private int _tier;
	private List<BoardPosition> _positions;

	public Combo() : this(BlockType.None) {}
	public Combo(BlockType type, int tier = 1) {
		_blockType = type;
		_tier = tier;
		_positions = new List<BoardPosition>(); 
	}

	public BlockType Type {
		get { return _blockType; }
		set { _blockType = value; }
	} 

	public int Tier {
		get { return _tier; }
		set { _tier = value; }
	}

    public List<BoardPosition> Positions {
        get { return _positions; }
    }

	public void Add(int row, int column) {
		Add(new BoardPosition(row, column));
	}

	public void Add(BoardPosition position) {
		if (!_positions.Contains(position)) {
			_positions.Add(position);
		}
	}

    public string ToString() {
        string s = "[";

        foreach(BoardPosition p in _positions) {
            s += p.ToString();
        }

        return s + "]";
    }

    public bool HasAnyPositionInCommon(Combo match) {
        // Checks if there is any position in common
        foreach(BoardPosition p1 in _positions) {
            foreach(BoardPosition p2 in match.Positions) {
                if (p1 == p2) {
                    return true;
                }
            }
        }

        return false;
    }

    public void AppendMatchPositions(Combo match) {
		foreach(BoardPosition p in match.Positions) {
			Add(p);
		}
    } 
}
