﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BoardPosition : System.IEquatable<BoardPosition> {
	public int row;
	public int column;

	public BoardPosition() {
		row = 0;
		column = 0;
	}

	public BoardPosition(int row, int column) {
		this.row = row;
		this.column = column;
	}

	public void SetPosition(int row, int column) {
		this.row = row;
		this.column = column;
	}

	public string ToString() {
		return "(" + row + ", " + column + ")";
	}

	public override bool Equals (object obj) {
		BoardPosition position = obj as BoardPosition;

		if (!object.ReferenceEquals(position, null)) {
			return this.Equals(position.row) && this.Equals(position.column);
		}
		
		return false;
	}
	
	public bool Equals(BoardPosition other) {
		if (other == null) {
			return false;
		}
		return ((row == other.row) && (column == other.column));
    }

    public static bool operator ==(BoardPosition p1, BoardPosition p2) {
		bool isP1Null = object.ReferenceEquals(p1, null);
		bool isP2Null = object.ReferenceEquals(p2, null);
		
		if (isP1Null && isP2Null) {
			return true;
		} else if (isP2Null) {
			return false;
		} else {
			return p1.Equals(p2);
		}
	}
	
	public static bool operator !=(BoardPosition p1, BoardPosition p2) {
        return !(p1 == p2);
    }
}
