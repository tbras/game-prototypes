using System;

public class BoardMove
{
	BoardPosition _fromPosition;
	BoardPosition _toPosition;

	public BoardMove (BoardPosition fromPosition, BoardPosition toPosition) {
		_fromPosition = fromPosition;
		_toPosition = toPosition;
	}

	public BoardMove (int[] fromPosition, int[] toPosition) {
		_fromPosition = new BoardPosition(fromPosition[0], fromPosition[1]);
		_toPosition = new BoardPosition(toPosition[0], toPosition[1]);
	}

	public BoardPosition FromPosition {
		get { return _fromPosition; }
		set { _fromPosition = value; }
	}

	public BoardPosition ToPosition {
		get { return _toPosition; }
		set { _toPosition = value; }
	}
}


