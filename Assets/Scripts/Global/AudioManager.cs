﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum SoundEffect { ComboGeneric, AttackSword };

[System.Serializable]
public class SoundEffectContainer {
	public SoundEffect identifier;
	public AudioClip audioClip;
	public float volumeScale = 1.0f;
}

[RequireComponent(typeof(AudioSource))]
public class AudioManager : MonoBehaviour 
{
	public List<SoundEffectContainer> soundEffectsContainer;

	private Dictionary<SoundEffect, SoundEffectContainer> _soundEffects;

	private static AudioManager _instance;
	
	public static AudioManager instance
	{
		get
		{
			if(_instance == null)
			{
				_instance = GameObject.FindObjectOfType<AudioManager>();
				
				//Tell unity not to destroy this object when loading a new scene!
				DontDestroyOnLoad(_instance.gameObject);
			}
			
			return _instance;
		}
	}
	
	void Awake() {
		if(_instance == null)
		{
			//If I am the first instance, make me the Singleton
			_instance = this;

			_soundEffects = new Dictionary<SoundEffect, SoundEffectContainer>();

			// Init dictionary
			for (int i = 0; i < soundEffectsContainer.Count; i++) {
				_soundEffects.Add(soundEffectsContainer[i].identifier, soundEffectsContainer[i]);
			}

			DontDestroyOnLoad(this);
		}
		else
		{
			//If a Singleton already exists and you find
			//another reference in scene, destroy it!
			if(this != _instance)
				Destroy(this.gameObject);
		}
	}
	
	public void PlaySoundEffect(SoundEffect soundEffect) {
		if (_soundEffects.ContainsKey(soundEffect)) {
			audio.PlayOneShot(_soundEffects[soundEffect].audioClip, _soundEffects[soundEffect].volumeScale);
		}
	}
}