﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class tk2dAnimationAdapter : MonoBehaviour {
	
	Color color = Color.white;
	Vector3 scale = Vector3.one;
	tk2dBaseSprite sprite = null;
	tk2dTextMesh textMesh = null;
	
	public Color TextColor = Color.white;
	public Vector3 TextScale = Vector3.one;
	
	// Use this for initialization
	void Start() {
		sprite = GetComponent<tk2dBaseSprite>();
		textMesh = GetComponent<tk2dTextMesh>();
		if (sprite != null) {
			color = sprite.color;
			scale = sprite.scale;
		}
		if (textMesh != null) {
			TextColor = textMesh.color;
			TextScale = textMesh.scale;
		}
	}
	
	// Update is called once per frame
	void LateUpdate () {
		DoUpdate();
	}
	
	void DoUpdate() {
		if (sprite != null && (sprite.color != color || sprite.scale != scale)) {
			color = sprite.color;
			scale = sprite.scale;
			sprite.Build();
		}
		if (textMesh != null && (textMesh.color != TextColor || textMesh.scale != TextScale)) {
			textMesh.color = TextColor;
			textMesh.scale = TextScale;
			textMesh.Commit();
		}
	}
}