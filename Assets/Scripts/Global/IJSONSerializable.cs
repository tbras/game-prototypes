using System;
using SimpleJSON;

public interface IJSONSerializable
{
	JSONNode Serialize();
	void Deserialize(JSONNode jsonRootNode);
}


