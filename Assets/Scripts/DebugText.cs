﻿using UnityEngine;
using System.Collections;

public class DebugText : MonoBehaviour {
	private static tk2dTextMesh textMesh;

	// Use this for initialization
	void Start () {
		textMesh = GetComponent<tk2dTextMesh>();

		if (textMesh == null)
			Debug.Log("NULL!");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public static void Display(string msg) {
		textMesh.text = msg;
//		textMesh.MakePixelPerfect();
		textMesh.Commit();
	}
}
