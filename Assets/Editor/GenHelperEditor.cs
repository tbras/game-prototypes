﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.IO;

[CustomEditor(typeof(GenHelper))]
public class GenHelperEditor : Editor {
	public override void OnInspectorGUI () {
		//Called whenever the inspector is drawn for this object.
		DrawDefaultInspector();
		//This draws the default screen.  You don't need this if you want
		//to start from scratch, but I use this when I'm just adding a button or
		//some small addition and don't feel like recreating the whole inspector.
		GenHelper myScript = (GenHelper)target;
		if(GUILayout.Button("Gen Sprite Enumerations")) {
			Debug.Log("App: " + Application.dataPath);

			DirectoryInfo spritesDirInfo = new DirectoryInfo(Application.dataPath + "/" + "Sprites");

			foreach(var info in spritesDirInfo.GetFiles("*.prefab")) {

			}
		}
	}
}
